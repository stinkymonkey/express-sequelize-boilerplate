const express = require('express')
const router = express.Router()
const db = require('../models')  

router.get('/all', (req, res) => {
    db.Employee.findAll().then(item => res.send(item))
})

module.exports = router