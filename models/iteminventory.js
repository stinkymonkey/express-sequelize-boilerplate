'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ItemInventory extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      this.belongsTo(models.Student, {
        foreignKey: 'studentId',
        as: 'student'
      })

      this.belongsTo(models.Employee, {
        foreignKey: 'employeeId',
        as: 'employee'
      })

      this.belongsTo(models.Item, {
        foreignKey: 'itemId',
        as: 'item'
      })
    }
  };
  ItemInventory.init({
    itemId: DataTypes.INTEGER,
    employeeId: DataTypes.INTEGER,
    studentId: DataTypes.INTEGER,
    createdBy: DataTypes.STRING,
    updatedBy: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'ItemInventory',
  });
  return ItemInventory;
};